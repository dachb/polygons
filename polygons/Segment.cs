﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using polygons.Constraints;

namespace polygons
{
    /// <summary>
    /// Klasa reprezentująca odcinek.
    /// </summary>
    public class Segment
    {
        /// <summary>
        /// Maksymalna odległość myszy od odcinka, dla której zaliczane jest kliknięcie.
        /// </summary>
        public static readonly int MaxDistance = 4;

        private IConstraint _constraint;

        /// <summary>
        /// Początek odcinka.
        /// </summary>
        public Vertex Start { get; set; }

        /// <summary>
        /// Koniec odcinka.
        /// </summary>
        public Vertex End { get; set; }

        public IConstraint Constraint
        {
            get { return _constraint; }
            set
            {
                if (value == null)
                {
                    _constraint = null;
                    return;
                }
                if (value.CanApply(this))
                {
                    _constraint = value;
                    _constraint.Apply(this);
                }
                else
                {
                    MessageBox.Show("Nie można dodać restrykcji: " + value.FailureMessage);
                }
            }
        }

        public int Length => (int)Math.Sqrt((Start.X - End.X) * (Start.X - End.X) + (Start.Y - End.Y) * (Start.Y - End.Y));

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="start">Początkowy wierzchołek.</param>
        /// <param name="end">Końcowy wierzchołek.</param>
        public Segment(Vertex start, Vertex end)
        {
            Start = start;
            End = end;
        }

        public Segment(int x1, int y1, int x2, int y2)
        {
            Start = new Vertex(x1, y1);
            End = new Vertex(x2, y2);
        }

        protected bool Equals(Segment other)
        {
            return Equals(Start, other.Start) && Equals(End, other.End);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Segment)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Start?.GetHashCode() ?? 0) * 397) ^ (End?.GetHashCode() ?? 0);
            }
        }


        /// <summary>
        /// Sprawdza, czy podana krawędź jest wystarczająco blisko odcinka,
        /// aby użytkownik mógł z nim wejść w interakcję.
        /// </summary>
        /// <param name="x">Współrzędna X wskaźnika myszki.</param>
        /// <param name="y">Współrzędna Y wskaźnika myszki.</param>
        /// <returns></returns>
        public bool CheckHit(int x, int y)
        {
            var A = End.Y - Start.Y;
            var B = End.X - Start.X;
            var C = End.X*Start.Y - End.Y*Start.X;
            double d = Math.Abs(A*x - B*y + C);
            return d * d <= MaxDistance*MaxDistance * (A*A + B*B) && InRectangle(x, y, this, MaxDistance / 2);
        }

        /// <summary>
        /// Rysuje odcinek, bez końców (bez wierzchołków).
        /// </summary>
        /// <param name="graphics">Obiekt grafiki, na którym powinno nastąpić rysowanie.</param>
        public void Paint(Bitmap bitmap, Graphics graphics)
        {
            Paint(bitmap);
            Constraint?.Paint(graphics, (Start.X + End.X) / 2, (Start.Y + End.Y) / 2);
        }

        public void Swap<T>(ref T first, ref T second)
        {
            var temp = first;
            first = second;
            second = temp;
        }

        public void Paint(Bitmap bitmap)
        {
            var color = Color.Black;
            var x0 = Start.X;
            var y0 = Start.Y;
            var x1 = End.X;
            var y1 = End.Y;
            var steep = Math.Abs(y0 - y1) >= Math.Abs(x0 - x1);
            if (steep)
            {
                Swap(ref x0, ref y0);
                Swap(ref x1, ref y1);
            }
            if (x0 > x1)
            {
                Swap(ref x0, ref x1);
                Swap(ref y0, ref y1);
            }
            var increment = y0 > y1 ? -1 : 1;
            var points = BresenhamLine(x0, y0, x1, y1, increment);
            foreach (var point in points)
            {
                if (steep) DrawInBounds(point.Y, point.X, bitmap, color);
                else DrawInBounds(point.X, point.Y, bitmap, color);
            }
        }

        public IEnumerable<Point> BresenhamLine(int x0, int y0, int x1, int y1, int increment)
        {
            var dx = x1 - x0;
            var dy = Math.Abs(y1 - y0);
            var d = 2*dy - dx;
            var x = x0;
            var y = y0;
            yield return new Point(x, y);
            while (x <= x1)
            {
                if (d <= 0)
                {
                    d += 2*dy;
                    x++;
                }
                else
                {
                    d += 2*(dy - dx);
                    x++;
                    y += increment;
                }
                yield return new Point(x, y);
            }
        }

        private static void DrawInBounds(int x, int y, Bitmap bitmap, Color color)
        {
            if (x >= 0 && y >= 0 &&
                x < bitmap.Size.Width && y < bitmap.Size.Height)
            {
                bitmap.SetPixel(x, y, color);
            }
        }

        /// <summary>
        /// Sprawdza, czy odcinek przecina się z podanym odcinkiem.
        /// </summary>
        /// <param name="other">Drugi odcinek do sprawdzenia.</param>
        /// <returns>Prawda, jeśli odcinki przecinają się; fałsz w przeciwnym wypadku.</returns>
        public bool Intersects(Segment other)
        {
            long d1 = Vertex.CrossProduct(other.End - other.Start, this.Start - other.Start);
            long d2 = Vertex.CrossProduct(other.End - other.Start, this.End - other.Start);
            long d3 = Vertex.CrossProduct(this.End - this.Start, other.Start - this.Start);
            long d4 = Vertex.CrossProduct(this.End - this.Start, other.End - this.Start);

            long d12 = d1*d2;
            long d34 = d3*d4;

            if (d12 > 0 || d34 > 0)
            {
                return false;
            }
            if (d12 < 0 || d34 < 0)
            {
                return true;
            }
            return InRectangle(this.Start, other) ||
                   InRectangle(this.End, other) ||
                   InRectangle(other.Start, this) ||
                   InRectangle(other.End, this);
        }

        /// <summary>
        /// Sprawdza, czy podany punkt znajduje się wewnątrz prostokąta
        /// wyznaczonego przez końce odcinka.
        /// </summary>
        /// <param name="vertex">Wierzchołek do sprawdzenia.</param>
        /// <param name="segment">Odcinek do sprawdzenia.</param>
        /// <returns>Prawda, jeśli punkt należy do prostokąta;
        /// fałsz w przeciwnym wypadku.</returns>
        private static bool InRectangle(Vertex vertex, Segment segment)
        {
            return InRectangle(vertex.X, vertex.Y, segment, 0);
        }

        /// <summary>
        /// Sprawdza, czy podany punkt znajduje się wewnątrz prostokąta
        /// wyznaczonego przez końce odcinka, z podaną tolerancją.
        /// </summary>
        /// <param name="x">Współrzędna X punktu.</param>
        /// <param name="y">Współrzędna Y punktu.</param>
        /// <param name="segment">Odcinek do sprawdzenia.</param>
        /// <param name="tolerance">Tolerancja w pikselach.</param>
        /// <returns></returns>
        private static bool InRectangle(int x, int y, Segment segment, int tolerance)
        {
            return Math.Min(segment.Start.X, segment.End.X) - tolerance <= x &&
                   x <= Math.Max(segment.Start.X, segment.End.X) + tolerance &&
                   Math.Min(segment.Start.Y, segment.End.Y) - tolerance <= y &&
                   y <= Math.Max(segment.Start.Y, segment.End.Y) + tolerance;
        }
    }
}
