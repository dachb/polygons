﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;

namespace polygons
{
    /// <summary>
    /// Klasa reprezentująca wierzchołek.
    /// </summary>
    public class Vertex
    {
        /// <summary>
        /// Współrzędna X wierzchołka.
        /// </summary>
        public int X => _x;
        /// <summary>
        /// Współrzędna Y wierzchołka.
        /// </summary>
        public int Y => _y;
        public bool First { get; }
        /// <summary>
        /// Określa, czy wierzchołek jest przesuwany, czy nie.
        /// </summary>
        public bool Moving { get; set; }

        /// <summary>
        /// Współrzędna X wierzchołka.
        /// </summary>
        private int _x;
        /// <summary>
        /// Współrzędna Y wierzchołka.
        /// </summary>
        private int _y;

        private Stack<Point> _storedStates;
        /// <summary>
        /// Rozmiar markera wierzchołka.
        /// </summary>
        private static readonly int MarkerSize = 7;

        public Vertex(int x, int y, bool first)
        {
            _x = x;
            _y = y;
            _storedStates = new Stack<Point>();
            First = first;
        }

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="x">Współrzędna X wierzchołka.</param>
        /// <param name="y">Współrzędna Y wierzchołka.</param>
        public Vertex(int x, int y) : this(x, y, false) { }

        public Vertex(Vertex vertex) : this(vertex.X, vertex.Y) { }

        /// <summary>
        /// Sprawdza, czy marker wierzchołka został trafiony przez użytkownika.
        /// </summary>
        /// <param name="x">Współrzędna X wierzchołka.</param>
        /// <param name="y">Współrzędna Y wierzchołka.</param>
        /// <returns>Prawda, jeśli marker został trafiony; fałsz w przeciwnym wypadku.</returns>
        public bool CheckHit(int x, int y)
        {
            return Math.Abs(_x - x) <= MarkerSize &&
                Math.Abs(_y - y) <= MarkerSize;
        }

        /// <summary>
        /// Przesuwa wierzchołek w podane współrzędne bezwzględne.
        /// </summary>
        /// <param name="x">Współrzędna X wierzchołka.</param>
        /// <param name="y">Współrzędna Y wierzchołka.</param>
        public void Move(int x, int y)
        {
            if (First) return;
            _x = x;
            _y = y;
        }

        /// <summary>
        /// Dokonuje translacji wierzchołka.
        /// </summary>
        /// <param name="dx">Pierwsza składowa wektora translacji.</param>
        /// <param name="dy">Druga składowa wektora translacji.</param>
        public void Translate(int dx, int dy)
        {
            _x += dx;
            _y += dy;
        }

        /// <summary>
        /// Rysuje wierzchołek.
        /// </summary>
        /// <param name="graphics">Obiekt grafiki, na którym powinno nastąpić rysowanie.</param>
        public void Paint(Graphics graphics)
        {
            Color color = Color.Black;
            if (Moving) color = Color.Red;
            if (First) color = Color.Blue;
            Rectangle rectangle = new Rectangle(
                _x - MarkerSize / 2, _y - MarkerSize / 2, MarkerSize, MarkerSize
            );
            using (SolidBrush brush = new SolidBrush(color))
            {
                graphics.FillRectangle(brush, rectangle);
            }
        }
        /// <summary>
        /// Wyznacza iloczyn skalarny wektorów o początku w (0,0) i końcach w podanych
        /// punktach.
        /// </summary>
        /// <param name="first">Pierwszy punkt.</param>
        /// <param name="second">Drugi punkt.</param>
        /// <returns>Iloczyn skalarny wektorów.</returns>
        public static int CrossProduct(Vertex first, Vertex second)
        {
            return first.X*second.Y - second.X*first.Y;
        }
        /// <summary>
        /// Oblicza różnicę dwóch wektorów zaczepionych w (0,0) o końcach w podanych punktach
        /// i zwraca drugi koniec różnicy.
        /// </summary>
        /// <param name="first">Pierwszy punkt.</param>
        /// <param name="second">Drugi punkt.</param>
        /// <returns>Drugi koniec różnicy.</returns>
        public static Vertex operator -(Vertex first, Vertex second)
        {
            return new Vertex(first.X - second.X, first.Y - second.Y);
        }

        public void Store()
        {
            _storedStates.Push(new Point(X, Y));
        }

        public void Restore()
        {
            var point = _storedStates.Pop();
            _x = point.X;
            _y = point.Y;
        }

        public void ClearStoredStates()
        {
            _storedStates.Clear();
        }
    }
}
