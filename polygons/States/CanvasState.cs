﻿using System.Windows.Forms;

namespace polygons.States
{
    /// <summary>
    /// Klasa abstrakcyjna dla stanów płótna.
    /// Służy uproszczeniu obsługi zdarzeń związanych z kliknięciem myszą.
    /// </summary>
    public abstract class CanvasState
    {
        /// <summary>
        /// Obsługuje wciśnięcie klawisza myszki.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public virtual void MouseDown(MouseEventArgs e) { }
        /// <summary>
        /// Obsługuje ruch myszki.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public virtual void MouseMove(MouseEventArgs e) { }
        /// <summary>
        /// Obsługuje zwolnienie klawisza myszki.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public virtual void MouseUp(MouseEventArgs e) { }
        /// <summary>
        /// Obsługuje wciśnięcie klawisza klawiatury.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public virtual void KeyDown(KeyEventArgs e) { }
        /// <summary>
        /// Obsługuje zwolnienie klawisza klawiatury.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public virtual void KeyUp(KeyEventArgs e) { }
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        public abstract bool IsDone { get; }
        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        public abstract CanvasState NextState { get; }
    }
}