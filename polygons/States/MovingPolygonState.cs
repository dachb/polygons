﻿using System.Windows.Forms;

namespace polygons.States
{
    /// <summary>
    /// Stan przesuwania wielokąta.
    /// </summary>
    public sealed class MovingPolygonState : CanvasState
    {
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        private bool _isDone;
        /// <summary>
        /// Współrzędna X ostatniego położenia myszy.
        /// </summary>
        private int _lastX;
        /// <summary>
        /// Współrzędna Y ostatniego położenia myszy.
        /// </summary>
        private int _lastY;
        /// <summary>
        /// Przesuwany wielokąt.
        /// </summary>
        private readonly Polygon _polygon;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="canvas">Płótno, na którym odbywa się rysowanie.</param>
        /// <param name="x">Współrzędna X ostatniego położenia myszy.</param>
        /// <param name="y">Współrzędna Y ostatniego położenia myszy.</param>
        /// <param name="polygon">Przesuwany wielokąt.</param>
        public MovingPolygonState(Canvas canvas, int x, int y, Polygon polygon)
        {
            NextState = new ShiftDownState(canvas);
            StoreCoordinates(x, y);
            _polygon = polygon;
        }

        /// <summary>
        /// Zachowuje podane współrzędne X, Y ostatniego położenia.
        /// </summary>
        /// <param name="x">Współrzędna X ostatniego położenia myszy.</param>
        /// <param name="y">Współrzędna Y ostatniego położenia myszy.</param>
        private void StoreCoordinates(int x, int y)
        {
            _lastX = x;
            _lastY = y;
        }
        /// <summary>
        /// Obsługuje ruch myszki.
        /// Dokonuje translacji wielokąta zgodnie z ruszami myszy.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void MouseMove(MouseEventArgs e)
        {
            int dx = e.X - _lastX;
            int dy = e.Y - _lastY;
            _polygon.Translate(dx, dy);
            StoreCoordinates(e.X, e.Y);
        }
        /// <summary>
        /// Obsługuje zwolnienie klawisza myszki.
        /// Kończy przesuwanie wielokąta.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void MouseUp(MouseEventArgs e)
        {
            _isDone = true;
        }

        public override void KeyUp(KeyEventArgs e)
        {
            if (!e.Shift)
            {
                _isDone = true;
            }
        }
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        public override bool IsDone => _isDone;
        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        public override CanvasState NextState { get; }
    }
}