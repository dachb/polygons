﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using polygons.Constraints;

namespace polygons.States
{
    /// <summary>
    /// Stan rysowania nowego wielokąta.
    /// </summary>
    public sealed class DrawingState : CanvasState
    {
        private readonly Canvas _canvas;
        /// <summary>
        /// Lista wielokątów.
        /// </summary>
        private readonly List<Polygon> _polygons;
        /// <summary>
        /// Obecnie rysowany wielokąt.
        /// </summary>
        private readonly Polygon _drawnPolygon;
        /// <summary>
        /// Pierwszy wierzchołek wielokąta.
        /// </summary>
        private readonly Vertex _firstVertex;
        /// <summary>
        /// Ostatnio dodany wierzchołek wielokąta.
        /// </summary>
        private Vertex _currentVertex;
        private Segment _currentSegment;
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        private bool _isDone;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="canvas">Płótno, na którym odbywa się rysowanie.</param>
        /// <param name="e">Obiekt zawierający szczegóły pierwszego kliknięcia.</param>
        public DrawingState(Canvas canvas, MouseEventArgs e)
        {
            _canvas = canvas;
            _polygons = canvas.Polygons;
            _drawnPolygon = new Polygon();
            CreateVertex(e, true);
            _firstVertex = _currentVertex;
            _polygons.Add(_drawnPolygon);
            NextState = new IdleState(canvas);
        }

        /// <summary>
        /// Obsługuje ruch myszki.
        /// Jeśli nie został stworzony następny wierzchołek, jest on tworzony.
        /// Jeśli następny wierzchołek istnieje, zostaje on przesunięty w położenie
        /// myszy.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void MouseMove(MouseEventArgs e)
        {
            if (_currentVertex == null)
            {
                CreateVertex(e, false);
            }
            else
            {
                _currentVertex.Move(e.X, e.Y);
                AddSuggestionIfNecessary();
            }
        }

        private void AddSuggestionIfNecessary()
        {
            List<IConstraint> toSuggest = new List<IConstraint> { new VerticalConstraint(), new HorizontalConstraint() };
            if (!_canvas.SuggestConstraints || _currentSegment == null) return;
            _currentSegment.Constraint = toSuggest.FirstOrDefault(c => c.CanApply(_currentSegment));
        }

        /// <summary>
        /// Tworzy wierzchołek zgodnie ze zdarzeniem i dodaje go do wielokąta.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        private void CreateVertex(MouseEventArgs e, bool first)
        {
            _currentVertex = new Vertex(e.X, e.Y, first);
            _currentSegment = _drawnPolygon.AddVertex(_currentVertex);
        }

        /// <summary>
        /// Obsługuje zwolnienie klawisza myszki.
        /// Jeżeli możliwe jest zamknięcie wielokąta, jest on zamykany.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void MouseUp(MouseEventArgs e)
        {
            if (_firstVertex.CheckHit(e.X, e.Y) && _drawnPolygon.CanClose())
            {
                _drawnPolygon.Close();
                _isDone = true;
            }
            ApplySuggestedConstraint();
            _currentVertex = null;
        }

        private void ApplySuggestedConstraint()
        {
            if (_currentSegment == null) return;
            if (_currentSegment.End.First)
            {
                _currentSegment.Constraint?.Apply(_currentSegment.End, _currentSegment.Start);
            }
            else
            {
                _currentSegment.Constraint?.Apply(_currentSegment);
            }
        }

        public override void KeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                _polygons.Remove(_drawnPolygon);
                _isDone = true;
            }
        }

        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        public override bool IsDone => _isDone;

        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        public override CanvasState NextState { get; }
    }
}