﻿using System.Windows.Forms;

namespace polygons.States
{
    /// <summary>
    /// Stan bierny płótna - nie następują żadne czynności.
    /// Punkt wyjścia aplikacji.
    /// </summary>
    public sealed class IdleState : CanvasState
    {
        /// <summary>
        /// Płótno, na którym odbywa się rysowanie.
        /// </summary>
        private readonly Canvas _canvas;
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        private bool _isDone;
        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        private CanvasState _nextState;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="canvas">Płótno, na którym odbywa się rysowanie.</param>
        public IdleState(Canvas canvas)
        {
            _canvas = canvas;
            _nextState = this;
            _isDone = false;
        }

        /// <summary>
        /// Obsługuje wciśnięcie klawisza myszki.
        /// Deleguje obsługę do właściwego podstanu.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void MouseDown(MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    _nextState = new LmbState(_canvas);
                    _isDone = true;
                    break;
                case MouseButtons.Right:
                    _nextState = new RmbState(_canvas);
                    _isDone = true;
                    break;
            }
        }

        /// <summary>
        /// Obsługuje wciśnięcie klawisza klawiatury.
        /// Jeśli wciśnięty został SHIFT, przechodzi w tryb przesuwania.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void KeyDown(KeyEventArgs e)
        {
            if (!e.Shift) return;
            _nextState = new ShiftDownState(_canvas);
            _isDone = true;
        }

        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        public override bool IsDone => _isDone;

        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        public override CanvasState NextState => _nextState;
    }
}