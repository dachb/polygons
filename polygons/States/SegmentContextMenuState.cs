﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using polygons.Constraints;
using polygons.States.Enums;

namespace polygons.States
{
    /// <summary>
    /// Stan pokazywania menu kontekstowego dla krawędzi.
    /// </summary>
    public class SegmentContextMenuState : CanvasState
    {
        /// <summary>
        /// Płótno, na którym odbywa się rysowanie.
        /// </summary>
        private readonly Canvas _canvas;
        /// <summary>
        /// Menu kontekstowe, które ma zostać wyświetlone.
        /// </summary>
        private readonly ContextMenuStrip _segmentContextMenu;
        /// <summary>
        /// Węzeł listy dwukierunkowej zawierający wybraną krawędź.
        /// </summary>
        private readonly LinkedListNode<Segment> _segment;
        /// <summary>
        /// Wielokąt, do którego należy wybrana krawędź.
        /// </summary>
        private readonly Polygon _segmentPolygon;
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        private bool _isDone;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="canvas">Płótno, na którym odbywa się rysowanie.</param>
        /// <param name="segment">Węzeł listy dwukierunkowej zawierający wybraną krawędź.</param>
        /// <param name="segmentPolygon">Wielokąt, do którego należy wybrana krawędź.</param>
        public SegmentContextMenuState(Canvas canvas, LinkedListNode<Segment> segment, Polygon segmentPolygon)
        {
            _canvas = canvas;
            NextState = new IdleState(canvas);
            _segment = segment;
            _segmentPolygon = segmentPolygon;
            _segmentContextMenu = CreateSegmentContextMenu();
        }
        /// <summary>
        /// Tworzy menu kontekstowe.
        /// </summary>
        /// <returns>Menu kontekstowe, które ma zostać wyświetlone.</returns>
        private ContextMenuStrip CreateSegmentContextMenu()
        {
            var contextMenuStrip = new ContextMenuStrip();
            contextMenuStrip.Items.AddRange(
                new ToolStripItem[]
                {
                    new ToolStripMenuItem("Podziel") {Tag = SegmentMenuItem.Bisect},
                    new ToolStripSeparator(),
                    new ToolStripMenuItem("Pionowy") {Tag = SegmentMenuItem.AddVerticalConstraint},
                    new ToolStripMenuItem("Poziomy") {Tag = SegmentMenuItem.AddHorizontalConstraint},
                    new ToolStripMenuItem("Ustal długość") {Tag = SegmentMenuItem.AddLengthConstraint},
                    new ToolStripMenuItem("Usuń ograniczenia") {Tag = SegmentMenuItem.ClearConstraints, Enabled = _segment.Value?.Constraint != null}
                }
            );
            contextMenuStrip.ItemClicked += SegmentContextMenuStripOnItemClicked;
            return contextMenuStrip;
        }

        public override void MouseDown(MouseEventArgs e)
        {
            CheckItem();
            _segmentContextMenu.Show(_canvas, e.X, e.Y);
        }

        private void CheckItem()
        {
            SegmentMenuItem? toCheck = null;
            var type = _segment.Value.Constraint?.GetType();
            if (type == typeof(HorizontalConstraint))
            {
                toCheck = SegmentMenuItem.AddHorizontalConstraint;
            }
            if (type == typeof(VerticalConstraint))
            {
                toCheck = SegmentMenuItem.AddVerticalConstraint;
            }
            if (type == typeof(LengthConstraint))
            {
                toCheck = SegmentMenuItem.AddLengthConstraint;
            }
            foreach (var toolStripItem in _segmentContextMenu.Items
                .OfType<ToolStripItem>()
                .Where(item => toCheck.Equals(item?.Tag)))
            {
                var toolStripMenuItem = toolStripItem as ToolStripMenuItem;
                if (toolStripMenuItem != null)
                {
                    toolStripMenuItem.Checked = true;
                }
            }
        }

        private void SegmentContextMenuStripOnItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            _segmentContextMenu.Hide();
            var itemType = e.ClickedItem.Tag as SegmentMenuItem?;
            if (!itemType.HasValue)
            {
                return;
            }
            switch (itemType.Value)
            {
                case SegmentMenuItem.Bisect:
                    _segmentPolygon.Bisect(_segment);
                    break;
                case SegmentMenuItem.AddHorizontalConstraint:
                    AddUniqueConstraint(new HorizontalConstraint());
                    break;
                case SegmentMenuItem.AddVerticalConstraint:
                    AddUniqueConstraint(new VerticalConstraint());
                    break;
                case SegmentMenuItem.AddLengthConstraint:
                    var dialog = new dlgLength(_segment.Value);
                    dialog.ShowDialog();
                    AddConstraint(new LengthConstraint(dialog.Length));
                    break;
                case SegmentMenuItem.ClearConstraints:
                    _segment.Value.Constraint = null;
                    break;
            }
        }

        private void AddUniqueConstraint(IConstraint constraint)
        {
            if (CheckNeighbors(_segment, constraint.GetType()))
            {
                AddConstraint(constraint);
            }
            else
            {
                MessageBox.Show("Nie można dodać restrykcji! Jedna z sąsiadujących krawędzi ma już ten typ restrykcji.");
            }
        }

        private void AddConstraint(IConstraint constraint)
        {
            _segmentPolygon.StoreVertexState();
            _segment.Value.Constraint = constraint;
            if (!_segmentPolygon.PropagateConstraints())
            {
                _segmentPolygon.RestoreVertexState();
                _segment.Value.Constraint = null;
                MessageBox.Show("Nie można dodać restrykcji! Obecne restrykcje nie mogą być jednocześnie spełnione.");
            }
        }

        private static bool CheckNeighbors(LinkedListNode<Segment> segment, Type type)
        {
            var prevConstraint = segment?.Previous?.Value.Constraint;
            var nextConstraint = segment?.Next?.Value.Constraint;
            return (prevConstraint == null || !type.IsInstanceOfType(prevConstraint)) &&
                   (nextConstraint == null || !type.IsInstanceOfType(nextConstraint));
        }

        public override void MouseUp(MouseEventArgs e)
        {
            _isDone = true;
        }

        public override bool IsDone => _isDone;

        public override CanvasState NextState { get; }
    }
}