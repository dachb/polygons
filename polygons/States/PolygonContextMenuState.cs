﻿using System;
using System.Windows.Forms;
using polygons.States.Enums;

namespace polygons.States
{
    public class PolygonContextMenuState : CanvasState
    {
        private Polygon _polygon;
        private Canvas _canvas;
        private ContextMenuStrip _polygonContextMenu;
        private bool _isDone;

        public PolygonContextMenuState(Canvas canvas, Polygon polygon)
        {
            NextState = new IdleState(canvas);
            _canvas = canvas;
            _polygon = polygon;
            _polygonContextMenu = CreatePolygonContextMenu();
        }

        private ContextMenuStrip CreatePolygonContextMenu()
        {
            var contextMenuStrip = new ContextMenuStrip();
            contextMenuStrip.Items.Add(
                new ToolStripMenuItem("Usuń") { Tag = PolygonMenuItem.Delete }
            );
            contextMenuStrip.ItemClicked += ContextMenuStripOnItemClicked;
            return contextMenuStrip;
        }

        private void ContextMenuStripOnItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            _polygonContextMenu.Hide();
            var clickedItem = e.ClickedItem.Tag as PolygonMenuItem?;
            if (!clickedItem.HasValue) return;
            switch (clickedItem.Value)
            {
                case PolygonMenuItem.Delete:
                    _canvas.Polygons.Remove(_polygon);
                    break;
            }
        }

        public override void MouseDown(MouseEventArgs e)
        {
            _polygonContextMenu.Show(_canvas, e.X, e.Y);
        }

        public override void MouseUp(MouseEventArgs e)
        {
            _isDone = true;
        }

        public override bool IsDone => _isDone;

        public override CanvasState NextState { get; }
    }
}