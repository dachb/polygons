﻿using System.Windows.Forms;

namespace polygons.States
{
    /// <summary>
    /// Stan poruszania wierzchołkiem.
    /// </summary>
    public sealed class MovingVertexState : CanvasState
    {
        private readonly Polygon _polygon;
        /// <summary>
        /// Przesuwany wierzchołek.
        /// </summary>
        private readonly Vertex _vertex;
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        private bool _isDone;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="canvas">Płótno, na którym odbywa się rysowanie.</param>
        /// <param name="polygon"></param>
        /// <param name="vertex">Przesuwany wierzchołek.</param>
        public MovingVertexState(Canvas canvas, Polygon polygon, Vertex vertex)
        {
            _vertex = vertex;
            _vertex.Moving = true;
            _polygon = polygon;
            _isDone = false;
            NextState = new IdleState(canvas);
            _polygon.StoreVertexState();
        }

        /// <summary>
        /// Obsługuje ruch myszki.
        /// Przesuwa punkt w pozycję myszki.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void MouseMove(MouseEventArgs e)
        {
            if (_vertex.First) return;
            _polygon.StoreVertexState();
            _vertex.Move(e.X, e.Y);
            if (!_polygon.PropagateConstraints())
            {
                _polygon.RestoreVertexState();
            }
        }
        /// <summary>
        /// Obsługuje zwolnienie klawisza myszki.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void MouseUp(MouseEventArgs e)
        {
            _isDone = true;
            _vertex.Moving = false;
        }
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        public override bool IsDone => _isDone;
        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        public override CanvasState NextState { get; }
    }
}