﻿using System.Linq;
using System.Windows.Forms;

namespace polygons.States
{
    /// <summary>
    /// Stan obsługi wciśnięcia lewego przycisku myszy.
    /// </summary>
    public class LmbState : CanvasState
    {
        /// <summary>
        /// Płótno, na którym odbywa się rysowanie.
        /// </summary>
        private readonly Canvas _canvas;
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        private bool _isDone;
        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        private CanvasState _nextState;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="canvas">Płótno, na którym odbywa się rysowanie.</param>
        public LmbState(Canvas canvas)
        {
            _canvas = canvas;
            _nextState = this;
            _isDone = false;
        }

        /// <summary>
        /// Obsługuje wciśnięcie klawisza myszki.
        /// Jeśli nie trafiono żadnego wierzchołka, rozpoczynamy rysowanie nowego
        /// wielokąta.
        /// Jeśli wierzchołek został trafiony, rozpoczynamy przesuwanie go.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void MouseDown(MouseEventArgs e)
        {
            Polygon polygon;
            var vertex = _canvas.HitVertex(e.X, e.Y, out polygon);
            if (vertex == null)
            {
                // no hit
                _nextState = new DrawingState(_canvas, e);
            }
            else
            {
                _nextState = new MovingVertexState(_canvas, polygon, vertex.Value);
            }
            _isDone = true;
        }

        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        public override bool IsDone => _isDone;

        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        public override CanvasState NextState => _nextState;
    }
}