﻿namespace polygons.States.Enums
{
    /// <summary>
    /// Wyliczenie odpowiadające elementom menu kontekstowego dla odcinków.
    /// </summary>
    public enum SegmentMenuItem
    {
        Bisect = 1,
        AddVerticalConstraint,
        AddHorizontalConstraint,
        AddLengthConstraint,
        ClearConstraints
    }
}
