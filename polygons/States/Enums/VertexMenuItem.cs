﻿namespace polygons.States.Enums
{
    /// <summary>
    /// Wyliczenie odpowiadające elementom menu kontekstowego dla wierzchołków.
    /// </summary>
    public enum VertexMenuItem
    {
        Delete = 1
    }
}
