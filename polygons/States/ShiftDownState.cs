﻿using System.Linq;
using System.Windows.Forms;

namespace polygons.States
{
    /// <summary>
    /// Stan przy wciśnięciu klawisza SHIFT.
    /// </summary>
    public sealed class ShiftDownState : CanvasState
    {
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        private bool _isDone;
        /// <summary>
        /// Płótno, na którym odbywa się rysowanie.
        /// </summary>
        private readonly Canvas _canvas;
        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        private CanvasState _nextState;
        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="canvas">Płótno, na którym odbywa się rysowanie.</param>
        public ShiftDownState(Canvas canvas)
        {
            _nextState = new IdleState(canvas);
            _canvas = canvas;
            _canvas.Cursor = Cursors.SizeAll;
        }
        /// <summary>
        /// Obsługuje zwolnienie klawisza klawiatury.
        /// Przywraca pierwotny kursor.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void KeyUp(KeyEventArgs e)
        {
            _canvas.Cursor = Cursors.Cross;
            _isDone = true;
        }
        /// <summary>
        /// Obsługuje wciśnięcie klawisza myszki.
        /// Jeśli trafione zostało wnętrze wielokąta, rozpoczyna jego przesuwanie.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void MouseDown(MouseEventArgs e)
        {
            var polygons = _canvas.Polygons;
            var polygon = polygons.LastOrDefault(poly => poly.ContainsPoint(e.X, e.Y));
            if (polygon != null)
            {
                _isDone = true;
                _nextState = new MovingPolygonState(_canvas, e.X, e.Y, polygon);
            }
        }

        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        public override bool IsDone => _isDone;
        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        public override CanvasState NextState => _nextState;
    }
}