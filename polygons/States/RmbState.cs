﻿using System.Windows.Forms;

namespace polygons.States
{
    /// <summary>
    /// Stan obsługi wciśnięcia prawego przycisku myszy.
    /// </summary>
    public class RmbState : CanvasState
    {
        /// <summary>
        /// Płótno, na którym odbywa się rysowanie.
        /// </summary>
        private readonly Canvas _canvas;
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        private bool _isDone;
        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        private CanvasState _nextState;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="canvas">Płótno, na którym odbywa się rysowanie.</param>
        public RmbState(Canvas canvas)
        {
            _isDone = false;
            _canvas = canvas;
            _nextState = new IdleState(canvas);
        }

        /// <summary>
        /// Obsługuje wciśnięcie klawisza myszki.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void MouseDown(MouseEventArgs e)
        {
            Polygon vertexPolygon, segmentPolygon;
            var polygon = _canvas.HitPolygon(e.X, e.Y);
            var vertex = _canvas.HitVertex(e.X, e.Y, out vertexPolygon);
            var segment = _canvas.HitSegment(e.X, e.Y, out segmentPolygon);
            if (polygon != null)
            {
                _nextState = new PolygonContextMenuState(_canvas, polygon);
                _isDone = true;
            }
            if (segment != null)
            {
                _nextState = new SegmentContextMenuState(_canvas, segment, segmentPolygon);
                _isDone = true;
            }
            if (vertex != null)
            {
                _nextState = new VertexContextMenuState(_canvas, vertex, vertexPolygon);
                _isDone = true;
            }
        }

        /// <summary>
        /// Obsługuje zwolnienie klawisza myszki.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public override void MouseUp(MouseEventArgs e)
        {
            _isDone = true;
        }
        /// <summary>
        /// Określa, czy można już przejść do kolejnego stanu.
        /// </summary>
        public override bool IsDone => _isDone;

        /// <summary>
        /// Określa, jaki powinien być nowy stan.
        /// </summary>
        public override CanvasState NextState => _nextState;
    }
}