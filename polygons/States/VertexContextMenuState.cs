﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using polygons.States.Enums;

namespace polygons.States
{
    public class VertexContextMenuState : CanvasState
    {
        private readonly ContextMenuStrip _vertexContextMenu;
        private readonly Canvas _canvas;
        private readonly LinkedListNode<Vertex> _vertex;
        private readonly Polygon _vertexPolygon;
        private bool _isDone;
        private CanvasState _nextState;

        public VertexContextMenuState(Canvas canvas, LinkedListNode<Vertex> vertex, Polygon vertexPolygon)
        {
            _nextState = new IdleState(canvas);
            _canvas = canvas;
            _vertex = vertex;
            _vertexPolygon = vertexPolygon;
            _vertexContextMenu = CreateVertexContextMenu();
        }

        private ContextMenuStrip CreateVertexContextMenu()
        {
            var contextMenuStrip = new ContextMenuStrip();
            contextMenuStrip.Items.Add(
                new ToolStripMenuItem("Usuń") { Tag = VertexMenuItem.Delete }
            );
            contextMenuStrip.ItemClicked += VertexContextMenuStripOnItemClicked;
            return contextMenuStrip;
        }

        private void VertexContextMenuStripOnItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            _vertexContextMenu.Hide();
            var itemType = e.ClickedItem.Tag as VertexMenuItem?;
            if (!itemType.HasValue)
            {
                return;
            }
            switch (itemType.Value)
            {
                case VertexMenuItem.Delete:
                    _vertexPolygon.DeleteVertex(_vertex);
                    break;
            }
        }

        public override void MouseDown(MouseEventArgs e)
        {
            if (_vertex.Value.First || _vertexPolygon.VertexCount <= 3)
            {
                _nextState = new PolygonContextMenuState(_canvas, _vertexPolygon);
                _isDone = true;
            }
            _vertexContextMenu.Show(_canvas, e.X, e.Y);
        }

        public override void MouseUp(MouseEventArgs e)
        {
            _isDone = true;
        }

        public override bool IsDone => _isDone;

        public override CanvasState NextState => _nextState;
    }
}