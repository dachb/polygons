﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace polygons
{
    public partial class dlgLength : Form
    {
        private int _length;

        public int Length => _length;

        public dlgLength(Segment segment)
        {
            InitializeComponent();
            _length = segment.Length;
            textBoxLength.Text = _length.ToString();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBoxLength.Text, out _length))
            {
                Close();
            }
            else {
                MessageBox.Show("Proszę podać długość jako liczbę całkowitą!");
            }
        }
    }
}
