﻿using System;
using System.Windows.Forms;
using polygons.States;

namespace polygons
{
    /// <summary>
    /// Zarządza stanami płótna.
    /// </summary>
    public class CanvasStateManager
    {
        /// <summary>
        /// Obecny stan płótna.
        /// </summary>
        private CanvasState _currentState;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="canvas">Płótno, na którym odbywa się rysowanie.</param>
        public CanvasStateManager(Canvas canvas)
        {
            _currentState = new IdleState(canvas);
        }
        /// <summary>
        /// Obsługuje przejścia stanów dla wciśnięcia klawisza myszy.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public void MouseDown(MouseEventArgs e)
        {
            IterateStates(cs => cs.MouseDown(e));
        }
        /// <summary>
        /// Obsługuje przejścia stanów dla ruchu myszy.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public void MouseMove(MouseEventArgs e)
        {
            IterateStates(cs => cs.MouseMove(e));
        }
        /// <summary>
        /// Obsługuje przejścia stanów dla zwolnienia klawisza myszy.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public void MouseUp(MouseEventArgs e)
        {
            IterateStates(cs => cs.MouseUp(e));
        }
        /// <summary>
        /// Obsługuje przejścia stanów dla wciśnięcia klawisza klawiatury.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public void KeyDown(KeyEventArgs e)
        {
            IterateStates(cs => cs.KeyDown(e));
        }
        /// <summary>
        /// Obsługuje przejścia stanów dla zwolnienia klawisza klawiatury.
        /// </summary>
        /// <param name="e">Obiekt zawierający szczegóły zdarzenia.</param>
        public void KeyUp(KeyEventArgs e)
        {
            IterateStates(cs => cs.KeyUp(e));
        }
        /// <summary>
        /// Wykonuje podaną akcję na stanach, a następnie sprawdza, czy stany
        /// zakończyły się. Jeśli tak, powtarza czynności; jeśli nie, pozostawia
        /// nieukończony stan.
        /// </summary>
        /// <param name="action">Akcja do wykonania.</param>
        private void IterateStates(Action<CanvasState> action)
        {
            while (true)
            {
                action(_currentState);
                if (!_currentState.IsDone)
                {
                    break;
                }
                _currentState = _currentState.NextState;
            }
        }
    }
}