﻿namespace polygons.Constraints
{
    /// <summary>
    /// Klasa z metodami rozszerzającymi dla interfejsu IConstraint.
    /// </summary>
    public static class ConstraintExtension
    {
        /// <summary>
        /// Zastosowuje restrykcję dla odcinka zgodnie z oznaczeniami jego wierzchołków
        /// (od początku odcinka do końca)
        /// </summary>
        /// <param name="constraint">Restrykcja do zastosowania.</param>
        /// <param name="segment">Odcinek, na którym powinna zostać zastosowana restrykcja.</param>
        public static void Apply(this IConstraint constraint, Segment segment)
        {
            constraint.Apply(segment.Start, segment.End);
        }
    }
}