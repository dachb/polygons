﻿using System;
using System.Drawing;

namespace polygons.Constraints
{
    /// <summary>
    /// Reprezentuje restrykcję, w której odcinek może być tylko poziomy.
    /// </summary>
    public class HorizontalConstraint : IConstraint
    {
        /// <summary>
        /// Rozmiar markera oznaczającego typ restrykcji na krawędzi.
        /// </summary>
        public const int MarkerSize = 20;
        /// <summary>
        /// Rozmiar ikony wewnątrz markera.
        /// </summary>
        public const int InnerSize = 12;
        /// <summary>
        /// Sprawdza, czy dana restrykcja może być zastosowana dla danego odcinka.
        /// </summary>
        /// <param name="segment">Odcinek, dla którego powinna zostać zastosowana restrykcja.</param>
        /// <returns>Prawda, jeśli restrykcja może zostać zastosowana; fałsz w przeciwnym przypadku.</returns>
        public bool CanApply(Segment segment)
        {
            var dx = Math.Abs(segment.Start.X - segment.End.X);
            var dy = Math.Abs(segment.Start.Y - segment.End.Y);
            return dx >= 4*dy;
        }
        /// <summary>
        /// Zastosowuje restrykcję dla odcinka, którego końcami są podane wierzchołki.
        /// Restrykcja jest stosowana od wierzchołka from do wierzchołka to.
        /// </summary>
        /// <param name="from">Wierzchołek startowy.</param>
        /// <param name="to">Wierzchołek, który ma być dopasowany do startowego,
        /// aby spełnić restrykcję.</param>
        public void Apply(Vertex from, Vertex to)
        {
            to.Move(to.X, from.Y);
        }
        /// <summary>
        /// Sprawdza, czy restrykcja jest spełniona.
        /// </summary>
        /// <param name="segment">Odcinek, który należy sprawdzić.</param>
        /// <returns>Prawda, jeśli restrykcja jest spełniona; fałsz w przeciwnym wypadku.</returns>
        public bool Verify(Segment segment)
        {
            return segment.Start.Y == segment.End.Y;
        }
        /// <summary>
        /// Rysuje symbol restrykcji o środku w podanych współrzędnych.
        /// </summary>
        /// <param name="graphics">Obiekt Graphics, za pomocą którego powinno odbyć się rysowanie.</param>
        /// <param name="x">Współrzędna X środka symbolu.</param>
        /// <param name="y">Współrzędna Y środka symbolu.</param>
        public void Paint(Graphics graphics, int x, int y)
        {
            var topLeftX = x - MarkerSize / 2;
            var topLeftY = y - MarkerSize / 2;
            using (Brush brush = new SolidBrush(Color.White))
            {
                graphics.FillEllipse(brush, topLeftX, topLeftY, MarkerSize, MarkerSize);
            }
            using (var pen = new Pen(Color.Red))
            {
                graphics.DrawEllipse(pen, topLeftX, topLeftY, MarkerSize, MarkerSize);
                graphics.DrawLine(pen, x - InnerSize / 2, y, x + InnerSize / 2, y);
            }
        }
        /// <summary>
        /// Treść błędu, który powinien pojawić się, gdy zastosowanie restrykcji jest niemożliwe.
        /// </summary>
        public string FailureMessage => "Odcinek jest zbyt pionowy.";
    }
}
