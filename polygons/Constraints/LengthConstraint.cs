﻿using System;
using System.Drawing;

namespace polygons.Constraints
{
    /// <summary>
    /// Reprezentuje restrykcję, w której odcinek może mieć tylko z góry określoną długość.
    /// </summary>
    public class LengthConstraint : IConstraint
    {
        /// <summary>
        /// Tolerancja w obliczaniu długości odcinka.
        /// </summary>
        private static readonly double Tolerance = Math.Sqrt(2) / 2;
        /// <summary>
        /// Wysokość znacznika restrykcji.
        /// </summary>
        private const int BoxHeight = 16;
        /// <summary>
        /// Szerokość znacznika restrykcji.
        /// </summary>
        private const int BoxWidth = 40;
        /// <summary>
        /// Zadana długość odcinka.
        /// </summary>
        private readonly int _length;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="length">Zadana długość odcinka.</param>
        public LengthConstraint(int length)
        {
            _length = length;
        }
        /// <summary>
        /// Sprawdza, czy dana restrykcja może być zastosowana dla danego odcinka.
        /// </summary>
        /// <param name="segment">Odcinek, dla którego powinna zostać zastosowana restrykcja.</param>
        /// <returns>Prawda, jeśli restrykcja może zostać zastosowana; fałsz w przeciwnym przypadku.</returns>
        public bool CanApply(Segment segment)
        {
            return _length > 0;
        }
        /// <summary>
        /// Zastosowuje restrykcję dla odcinka, którego końcami są podane wierzchołki.
        /// Restrykcja jest stosowana od wierzchołka from do wierzchołka to.
        /// </summary>
        /// <param name="from">Wierzchołek startowy.</param>
        /// <param name="to">Wierzchołek, który ma być dopasowany do startowego,
        /// aby spełnić restrykcję.</param>
        public void Apply(Vertex from, Vertex to)
        {
            double dx = to.X - from.X;
            double dy = to.Y - from.Y;
            var oldLengthSquared = dx*dx + dy*dy;
            double newLengthSquared = _length*_length;
            var scale = Math.Sqrt(newLengthSquared/oldLengthSquared);
            dx *= scale;
            dy *= scale;
            to.Move(from.X + (int)Math.Round(dx), from.Y + (int)Math.Round(dy));
        }
        /// <summary>
        /// Sprawdza, czy restrykcja jest spełniona.
        /// </summary>
        /// <param name="segment">Odcinek, który należy sprawdzić.</param>
        /// <returns>Prawda, jeśli restrykcja jest spełniona; fałsz w przeciwnym wypadku.</returns>
        public bool Verify(Segment segment)
        {
            var dx = segment.Start.X - segment.End.X;
            var dy = segment.Start.Y - segment.End.Y;
            var minR = _length - Tolerance;
            var maxR = _length + Tolerance;
            return dx*dx + dy*dy >= minR*minR &&
                   dx*dx + dy*dy <= maxR*maxR;
        }
        /// <summary>
        /// Rysuje symbol restrykcji o środku w podanych współrzędnych.
        /// </summary>
        /// <param name="graphics">Obiekt Graphics, za pomocą którego powinno odbyć się rysowanie.</param>
        /// <param name="x">Współrzędna X środka symbolu.</param>
        /// <param name="y">Współrzędna Y środka symbolu.</param>
        public void Paint(Graphics graphics, int x, int y)
        {
            var box = new RectangleF(x - BoxWidth / 2, y - BoxHeight / 2, BoxWidth, BoxHeight);
            var font = new Font(FontFamily.GenericMonospace, 8);
            StringFormat stringFormat = new StringFormat
            {
                LineAlignment = StringAlignment.Center,
                Alignment = StringAlignment.Center
            };
            using (Brush brush = new SolidBrush(Color.White))
            {
                graphics.FillEllipse(brush, box);
            }
            using (Pen pen = new Pen(Color.Red))
            {
                graphics.DrawEllipse(pen, box.X, box.Y, box.Width, box.Height);
            }
            using (Brush brush = new SolidBrush(Color.Red))
            {
                graphics.DrawString(_length.ToString(), font, brush, box, stringFormat);
            }
        }
        /// <summary>
        /// Treść błędu, który powinien pojawić się, gdy zastosowanie restrykcji jest niemożliwe.
        /// </summary>
        public string FailureMessage => "Długość odcinka musi być liczbą nieujemną.";
    }
}
