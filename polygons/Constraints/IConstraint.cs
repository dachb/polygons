﻿using System.Drawing;

namespace polygons.Constraints
{
    /// <summary>
    /// Interfejs reprezentujący restrykcje dla odcinków.
    /// </summary>
    public interface IConstraint
    {
        /// <summary>
        /// Sprawdza, czy dana restrykcja może być zastosowana dla danego odcinka.
        /// </summary>
        /// <param name="segment">Odcinek, dla którego powinna zostać zastosowana restrykcja.</param>
        /// <returns>Prawda, jeśli restrykcja może zostać zastosowana; fałsz w przeciwnym przypadku.</returns>
        bool CanApply(Segment segment);
        /// <summary>
        /// Zastosowuje restrykcję dla odcinka, którego końcami są podane wierzchołki.
        /// Restrykcja jest stosowana od wierzchołka from do wierzchołka to.
        /// </summary>
        /// <param name="from">Wierzchołek startowy.</param>
        /// <param name="to">Wierzchołek, który ma być dopasowany do startowego,
        /// aby spełnić restrykcję.</param>
        void Apply(Vertex from, Vertex to);
        /// <summary>
        /// Sprawdza, czy restrykcja jest spełniona.
        /// </summary>
        /// <param name="segment">Odcinek, który należy sprawdzić.</param>
        /// <returns>Prawda, jeśli restrykcja jest spełniona; fałsz w przeciwnym wypadku.</returns>
        bool Verify(Segment segment);
        /// <summary>
        /// Rysuje symbol restrykcji o środku w podanych współrzędnych.
        /// </summary>
        /// <param name="graphics">Obiekt Graphics, za pomocą którego powinno odbyć się rysowanie.</param>
        /// <param name="x">Współrzędna X środka symbolu.</param>
        /// <param name="y">Współrzędna Y środka symbolu.</param>
        void Paint(Graphics graphics, int x, int y);

        /// <summary>
        /// Treść błędu, który powinien pojawić się, gdy zastosowanie restrykcji jest niemożliwe.
        /// </summary>
        string FailureMessage { get; }
    }
}
