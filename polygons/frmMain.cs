﻿using System.Windows.Forms;

namespace polygons
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void mnuSuggestConstraintsItem_CheckedChanged(object sender, System.EventArgs e)
        {
            cnvCanvas.SuggestConstraints = mnuSuggestConstraintsItem.Checked;
            toolStripStatusLabel.Text = mnuSuggestConstraintsItem.Checked ? "Sugestie ograniczeń włączone" : "";
        }
    }
}
