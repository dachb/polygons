﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using polygons.Constraints;

namespace polygons
{
    /// <summary>
    /// Klasa reprezentująca wielokąt.
    /// </summary>
    public class Polygon
    {
        /// <summary>
        /// Lista wierzchołków wielokąta.
        /// </summary>
        private readonly LinkedList<Vertex> _vertices;
        /// <summary>
        /// Lista odcinków wielokąta.
        /// </summary>
        private readonly LinkedList<Segment> _segments;
        /// <summary>
        /// Ostatnio dodany wierzchołek.
        /// </summary>
        private Vertex _lastVertex;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        public Polygon()
        {
            _vertices = new LinkedList<Vertex>();
            _segments = new LinkedList<Segment>();
        }

        /// <summary>
        /// Dodaje wierzchołek do wielokąta.
        /// </summary>
        /// <param name="vertex">Wierzchołek do dodania.</param>
        public Segment AddVertex(Vertex vertex)
        {
            _vertices.AddLast(vertex);
            if (_lastVertex != null)
            {
                _segments.AddLast(new Segment(_lastVertex, vertex));
            }
            _lastVertex = vertex;
            return _segments.Last?.Value;
        }

        /// <summary>
        /// Określa, czy można zamknąć wielokąt.
        /// </summary>
        /// <returns>Prawda, jeżeli wielokąt ma co najmniej 3 wierzchołki.</returns>
        public bool CanClose()
        {
            return _vertices.Count >= 3;
        }

        /// <summary>
        /// Rysuje wielokąt.
        /// </summary>
        /// <param name="graphics">Obiekt grafiki, na którym powinno nastąpić rysowanie.</param>
        /// <param name="bitmap"></param>
        public void Paint(Graphics graphics, Bitmap bitmap)
        {
            foreach (var vertex in _vertices)
            {
                vertex.Paint(graphics);
            }
            foreach (var segment in _segments)
            {
                segment.Paint(bitmap, graphics);
            }
        }

        public LinkedListNode<Vertex> FindVertex(int x, int y)
        {
            var iterator = _vertices.First;
            LinkedListNode<Vertex> hitNode = null;
            while (iterator != null)
            {
                if (iterator.Value.CheckHit(x, y))
                {
                    hitNode = iterator;
                }
                iterator = iterator.Next;
            }
            return hitNode;
        }
        
        public LinkedListNode<Segment> FindSegment(int x, int y)
        {
            var iterator = _segments.First;
            LinkedListNode<Segment> hitNode = null;
            while (iterator != null)
            {
                if (iterator.Value.CheckHit(x, y))
                {
                    hitNode = iterator;
                }
                iterator = iterator.Next;
            }
            return hitNode;
        }

        /// <summary>
        /// Zamyka wielokąt.
        /// </summary>
        public void Close()
        {
            _vertices.RemoveLast();
            _segments.Last.Value.End = _vertices.First.Value;
        }

        /// <summary>
        /// Sprawdza, czy dany punkt leży wewnątrz danego wielokąta.
        /// </summary>
        /// <param name="x">Współrzędna X punktu.</param>
        /// <param name="y">Współrzędna Y punktu.</param>
        /// <returns>Prawda, jeśli punkt zawarty jest w wielokącie;
        /// fałsz w przeciwnym wypadku.</returns>
        public bool ContainsPoint(int x, int y)
        {
            var ray = new Segment(x, y, 10000, y);
            var vertexHits = _vertices.Count(v => v.Y == y && v.X >= x);
            // Nie liczymy wierzchołków, kursor musi być wewnątrz wielokąta
            var hits = _segments.Count(s => s.Intersects(ray)) - vertexHits;
            return hits%2 == 1;
        }

        /// <summary>
        /// Dokonuje translacji wierzchołków wielokąta.
        /// </summary>
        /// <param name="dx">Różnica na współrzędnej X.</param>
        /// <param name="dy">Różnica na współrzędnej Y.</param>
        public void Translate(int dx, int dy)
        {
            foreach (var v in _vertices)
            {
                v.Translate(dx, dy);
            }
        }
        
        public void Bisect(LinkedListNode<Segment> segmentNode)
        {
            var segment = segmentNode.Value;
            var startNode = _vertices.Find(segment.Start);
            if (!_segments.Contains(segment) || startNode == null)
            {
                return;
            }
            var midpoint = new Vertex(
                (segment.Start.X + segment.End.X) / 2,
                (segment.Start.Y + segment.End.Y) / 2
            );
            _vertices.AddAfter(startNode, midpoint);
            _segments.AddBefore(segmentNode, new Segment(segment.Start, midpoint));
            _segments.AddAfter(segmentNode, new Segment(midpoint, segment.End));
            _segments.Remove(segment);
        }

        public void DeleteVertex(LinkedListNode<Vertex> vertexNode)
        {
            if (vertexNode == null) return;
            var prevNode = vertexNode.Previous ?? _vertices.Last;
            var nextNode = vertexNode.Next ?? _vertices.First;
            var segmentNode = _segments.Find(new Segment(prevNode.Value, vertexNode.Value));
            if (segmentNode == null) return;
            segmentNode.Value.End = nextNode.Value;
            segmentNode.Value.Constraint = null;
            _segments.Remove(new Segment(vertexNode.Value, nextNode.Value));
            _vertices.Remove(vertexNode);
        }

        public bool PropagateConstraints()
        {
            var nonConstrained = new List<LinkedListNode<Segment>>();
            var directionConstrained = new List<LinkedListNode<Segment>>();
            var lengthConstrained = new List<LinkedListNode<Segment>>();
            for (var iterator = _segments.First; iterator != null; iterator = iterator.Next)
            {
                if (iterator.Value.Constraint == null)
                {
                    nonConstrained.Add(iterator);
                }
                if (iterator.Value.Constraint?.GetType() == typeof(LengthConstraint))
                {
                    lengthConstrained.Add(iterator);
                }
                else
                {
                    directionConstrained.Add(iterator);
                }
            }
            return nonConstrained.Concat(directionConstrained).Concat(lengthConstrained).Any(TryPropagation);
        }

        private bool TryPropagation(LinkedListNode<Segment> segmentNode)
        {
            StoreVertexState();
            var forwardIterator = _segments.First;
            var backwardIterator = _segments.Last;
            while (forwardIterator != null && forwardIterator != segmentNode)
            {
                forwardIterator.Value.Constraint?.Apply(forwardIterator.Value.Start, forwardIterator.Value.End);
                forwardIterator = forwardIterator.Next;
            }
            while (backwardIterator != null && backwardIterator != segmentNode.Next)
            {
                backwardIterator.Value.Constraint?.Apply(backwardIterator.Value.End, backwardIterator.Value.Start);
                backwardIterator = backwardIterator.Previous;
            }
            var okay = _segments.All(s => s.Constraint?.Verify(s) ?? true);
            if (!okay) RestoreVertexState();
            else ClearVertexState();
            return okay;
        }

        public void StoreVertexState()
        {
            foreach (var vertex in _vertices)
            {
                vertex.Store();
            }
        }

        public void RestoreVertexState()
        {
            foreach (var vertex in _vertices)
            {
                vertex.Restore();
            }
        }

        public void ClearVertexState()
        {
            foreach (var vertex in _vertices)
            {
                vertex.ClearStoredStates();
            }
        }

        public int VertexCount => _vertices.Count;
    }
}
