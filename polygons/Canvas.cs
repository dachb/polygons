﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace polygons
{
    /// <summary>
    /// Kontrolka "płótna" do rysowania kształtów.
    /// </summary>
    public partial class Canvas : UserControl
    {
        /// <summary>
        /// Menedżer stanów płótna.
        /// </summary>
        private readonly CanvasStateManager _stateManager;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        public Canvas()
        {
            InitializeBitmap();
            InitializeComponent();
            Polygons = new List<Polygon>();
            _stateManager = new CanvasStateManager(this);
        }
        
        /// <summary>
        /// Tworzy bitmapę o odpowiednim kolorze, a następnie wypełnia ją jednolitym kolorem.
        /// </summary>
        private void InitializeBitmap()
        {
            Bitmap oldBitmap = Bitmap;
            oldBitmap?.Dispose();
            Bitmap = new Bitmap(Size.Width, Size.Height);
            using (Graphics graphics = Graphics.FromImage(Bitmap))
            {
                PaintBackground(graphics);
            }
        }

        /// <summary>
        /// Wypełnia podany obiekt grafiki jednolitym kolorem.
        /// </summary>
        /// <param name="graphics">Obiekt grafiki, który należy wypełnić.</param>
        private void PaintBackground(Graphics graphics)
        {
            using (SolidBrush brush = new SolidBrush(Color.White))
            {
                graphics.FillRectangle(brush, new Rectangle(
                    0, 0, Size.Width, Size.Height
                ));
            }
        }

        /// <summary>
        /// Wykonuje czynności związane ze zmianą rozmiaru kontrolki.
        /// </summary>
        /// <param name="sender">Nadawca zdarzenia.</param>
        /// <param name="e">Argumenty zdarzenia.</param>
        private void Canvas_SizeChanged(object sender, EventArgs e)
        {
            if (Size.Height == 0 || Size.Width == 0) return;
            InitializeBitmap();
        }

        /// <summary>
        /// Wykonuje czynności związane ze zwolnieniem przycisku myszki.
        /// </summary>
        /// <param name="sender">Nadawca zdarzenia.</param>
        /// <param name="e">Argumenty zdarzenia.</param>
        private void Canvas_MouseUp(object sender, MouseEventArgs e)
        {
            _stateManager.MouseUp(e);
            Invalidate();
        }
        /// <summary>
        /// Wykonuje czynności związane ze ruchem myszki.
        /// </summary>
        /// <param name="sender">Nadawca zdarzenia.</param>
        /// <param name="e">Argumenty zdarzenia.</param>
        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            _stateManager.MouseMove(e);
            Invalidate();
        }
        /// <summary>
        /// Wykonuje czynności związane ze wciśnięciem przycisku myszki.
        /// </summary>
        /// <param name="sender">Nadawca zdarzenia.</param>
        /// <param name="e">Argumenty zdarzenia.</param>
        private void Canvas_MouseDown(object sender, MouseEventArgs e)
        {
            _stateManager.MouseDown(e);
            Invalidate();
        }
        /// <summary>
        /// Wykonuje czynności związane ze wciśnięciem klawisza klawiatury.
        /// </summary>
        /// <param name="sender">Nadawca zdarzenia.</param>
        /// <param name="e">Argumenty zdarzenia.</param>
        private void Canvas_KeyDown(object sender, KeyEventArgs e)
        {
            _stateManager.KeyDown(e);
            Invalidate();
        }
        /// <summary>
        /// Wykonuje czynności związane ze zwolnieniem klawisza klawiatury.
        /// </summary>
        /// <param name="sender">Nadawca zdarzenia.</param>
        /// <param name="e">Argumenty zdarzenia.</param>
        private void Canvas_KeyUp(object sender, KeyEventArgs e)
        {
            _stateManager.KeyUp(e);
            Invalidate();
        }
        /// <summary>
        /// Rysuje zawartość kontrolki.
        /// </summary>
        /// <param name="sender">Nadawca zdarzenia.</param>
        /// <param name="e">Argumenty zdarzenia.</param>
        private void Canvas_Paint(object sender, PaintEventArgs e)
        {
            using (Graphics graphics = Graphics.FromImage(Bitmap))
            {
                PaintBackground(graphics);
                foreach (var polygon in Polygons)
                {
                    polygon.Paint(graphics, Bitmap);
                }
            }
            e.Graphics.DrawImage(Bitmap, new Point(0, 0));
        }
        /// <summary>
        /// Bitmapa, na której odbywa się rysowanie.
        /// </summary>
        private Bitmap Bitmap { get; set; }
        /// <summary>
        /// Wszystkie dotychczasowo narysowane wielokąty.
        /// </summary>
        public List<Polygon> Polygons { get; }

        public LinkedListNode<Vertex> HitVertex(int x, int y, out Polygon polygon)
        {
            polygon = Polygons.LastOrDefault(poly => poly.FindVertex(x, y) != null);
            return polygon?.FindVertex(x, y);
        }

        public LinkedListNode<Segment> HitSegment(int x, int y, out Polygon polygon)
        {
            polygon = Polygons.LastOrDefault(poly => poly.FindSegment(x, y) != null);
            return polygon?.FindSegment(x, y);
        }

        public Polygon HitPolygon(int x, int y)
        {
            return Polygons.LastOrDefault(poly => poly.ContainsPoint(x, y));
        }

        public bool SuggestConstraints { get; set; }
    }
}
