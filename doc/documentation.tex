\documentclass[10pt,a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{float}
\usepackage{helvet}
\usepackage{fullpage}
\usepackage{keystroke}
\usepackage{hyperref}
\usepackage{algorithm}
\usepackage{algorithmicx}
\usepackage{algpseudocode}
\usepackage[polish]{babel}
\usepackage{polski}

\floatname{algorithm}{Algorytm}
\renewcommand\familydefault{\sfdefault}

\begin{document}
    \title{Edytor wielokątów -- dokumentacja}
    \author{Bartłomiej Dach}
    \maketitle

\abstract
Poniższy dokument zawiera opis edytora wielokątów \emph{Polygons}, instrukcję obsługi programu oraz szczegóły implementacyjne działania mechanizmu restrykcji dla krawędzi, takich, jak: ustalenie krawędzi jako pionowej, poziomej lub ustalenie długości krawędzi, oraz zastosowanie mechanizmu podczas edycji pozycji wierzchołków danego wielokąta.

\section{Instrukcja obsługi}
Program \emph{Polygons} jest prostym narzędziem do edycji grafiki wektorowej z interfejsem \emph{kliknij-i-przeciągnij} (ang. \emph{drag-and-drop}). Całość okna aplikacji stanowi obszar roboczy, na którym następuje rysowanie wielokątów.
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{main-window.PNG}
    \caption{Wygląd głównego okna aplikacji}
\end{figure}

\subsection{Rysowanie wielokątów}
Aby rozpocząć rysowanie wielokąta, należy kliknąć lewym przyciskiem myszy w dowolnym miejscu obszaru roboczego, który nie jest wierzchołkiem już istniejącego wielokąta. W momencie kliknięcia przycisku na ekranie pojawi się znacznik pierwszego wierzchołka, oznaczony kolorem niebieskim, oraz znacznik kolejnego wierzchołka. Odcinki rysowane są między kolejnymi wierzchołkami na bieżąco.
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{drawing.png}
    \caption{Proces rysowania wielokątów}
\end{figure}
Kolejne kliknięcia lewego przycisku myszy powodują zatwierdzenie pozycji obecnie rysowanego wierzchołka. Aby zakończyć rysowanie, należy zamknąć wielokąt, klikając na niebieski znacznik pierwszego wierzchołka. Rysowanie można też zakończyć wcześnie w razie pomyłki, wciskając klawisz \Esc.

\paragraph{Uwaga:}
Wielokąta nie można zamknąć, jeśli ma mniej niż 3 wierzchołki.

\subsection{Przesuwanie wierzchołków wielokąta}
Aby przesunąć dowolny z wierzchołków wielokąta, należy kliknąć w odpowiadający temu wierzchołkowi znacznik, który powinien zmienić kolor na czerwony, a następnie przeciągnąć kursor myszy w żądane położenie.
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{moving-vertex.png}
    \caption{Przeciąganie wierzchołka wielokąta}
\end{figure}

\paragraph{Uwaga:}
Przesunięcie pierwszych wierzchołków poszczególnych wielokątów (oznaczonych na niebiesko) jest niemożliwe.

\subsection{Przesuwanie wielokąta}
Aby przesunąć wielokąt, należy przytrzymać klawisz \Shift i przeciągnąć wybrany wielokąt w żądane miejsce.
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{moving-polygon.png}
    \caption{Przesuwanie pozycji wielokąta}
\end{figure}

\paragraph{Uwaga:}
W przypadku, gdy kursor znajduje się nad wieloma wielokątami, przesuwany jest ostatnio dodana figura (\emph{z-order} to kolejność dodawania).

\subsection{Usuwanie wierzchołka}
Aby usunąć dany wierzchołek, należy wcisnąć prawy przycisk myszy na znaczniku wierzchołka i wybrać opcję menu \keystroke{Usuń}.
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{delete-vertex.png}
    \caption{Usuwanie wierzchołka}
\end{figure}

\paragraph{Uwaga:}
Jeżeli wybrany wierzchołek był startowy (oznaczony na niebiesko) lub należy do wielokąta, który ma dokładnie 3 wierzchołki, usuwany jest cały wielokąt.

\subsection{Usuwanie wielokąta}
Aby usunąć dany wielokąt, należy wcisnąć prawy przycisk myszy wewnątrz wielokąta i wybrać opcję menu \keystroke{Usuń}.
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{delete-polygon.png}
    \caption{Usuwanie wielokąta}
\end{figure}

\subsection{Dodawanie wierzchołka na środku krawędzi}
Aby dodać wierzchołek pośrodku wybranej krawędzi, należy wcisnąć prawy przycisk myszy na krawędzi i wybrać opcję \keystroke{Podziel}.
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{bisect.png}
    \caption{Dzielenie krawędzi}
\end{figure}

\subsection{Dodawanie ograniczeń krawędzi}
Każdej krawędzi może być przypisane jedno z ograniczeń:
\begin{itemize}
    \item krawędź jest pionowa,
    \item krawędź jest pozioma,
    \item krawędź ma ustaloną długość.
\end{itemize}

\subsubsection{Krawędzie pionowe lub poziome}
Aby dodać restrykcję na pionowy lub poziomy kierunek krawędzi, należy kliknąć prawym przyciskiem na żądaną krawędź i wybrać opcje odpowiednio: \keystroke{Pionowy} lub \keystroke{Poziomy}.
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{constraint-before.png}
    \caption{Dodawanie restrykcji}
\end{figure}
Po dodaniu ograniczenie zostanie zastosowane lub pojawi się komunikat o błędzie. Możliwe powody błędów to:
\begin{itemize}
    \item krawędź ma zbyt duże odchylenie od pionu (poziomu),
    \item jedna z sąsiadujących krawędzi ma już ten sam typ restrykcji,
    \item restrykcja nie może być zastosowana z powodu sąsiadujących ograniczeń.
\end{itemize}
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{constraint-after.png}
    \caption{Wynik dodania ograniczenia}
\end{figure}

\subsubsection{Krawędzie określonej długości}
Aby ustalić długość krawędzi, należy kliknąć prawym przyciskiem na żądaną krawędź i wybrać opcję \keystroke{Ustal długość}.
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{constraint-length-menu.png}
    \caption{Pozycja opcji w menu}
\end{figure}
Po wybraniu opcji pojawi się okno dialogowe, w którym należy wpisać żądaną długość krawędzi. Domyślną wartością jest zaokrąglona do pełnej liczby dotychczasowa długość odcinka.
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{constraint-length-specify.png}
    \caption{Specyfikacja długości odcinka}
\end{figure}
Po kliknięciu OK ograniczenie zostanie zastosowane lub pojawi się komunikat o błędzie spowodowany jednym z poniższych:
\begin{itemize}
    \item podana długość jest ujemna,
    \item restrykcja nie może być zastosowana z powodu sąsiadujących ograniczeń.
\end{itemize}
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{constraint-length-after.png}
    \caption{Wynik zastosowanej restrykcji}
\end{figure}
Dodane ograniczenie można usunąć, wybierając opcję \keystroke{Usuń ograniczenia} w menu kontekstowym.
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{remove-constraint.png}
    \caption{Opcja usuwania restrykcji}
\end{figure}

\section{Algorytm zastosowania restrykcji}
Ograniczenia krawędzi są obiektami; każda krawędź wielokąta może mieć przypisane co najwyżej jedno ograniczenie. Kluczową metodą obiektów restrykcji jest metoda \textsc{Apply($v_1$, $v_2$)} przyjmująca dwa wierzchołki odcinka, która przesuwa wierzchołek $v_2 = (x_2, y_2)$, aby restrykcja była spełniona względem pierwszego wierzchołka $v_1 = (x_1, y_1)$, to jest:
\begin{itemize}
    \item Dla restrykcji pionowej, punkt $v_2 = (x_2, y_2)$ jest przesunięty na pozycję $(x_1, y_2)$.
    \item Dla restrykcji poziomej, punkt $v_2 = (x_2, y_2)$ jest przesunięty na pozycję $(x_2, y_1)$.
    \item Dla ustalonej długości krawędzi $r$, punkt $v_2$ jest punktem przecięcia prostej wyznaczonej przez odcinek $v_1 v_2$ oraz koła o środku w $v_1$ i środku $r$.
\end{itemize}
W momencie dodawania restrykcji oraz przesuwania wierzchołków następuje właściwy algorytm, który próbuje dopasować pozycje wierzchołków tak, aby wszystkie restrykcje były spełnione. Pseudokod tego algorytmu jest przedstawiony poniżej:
\begin{algorithm}[H]
\begin{algorithmic}
\Function{Propagate}{krawędź}
    \State{iter $\gets$ pierwsza krawędź wielokąta}
    \While{iter $\neq$ null $\wedge$ iter $\neq$ krawędź}
        \If{krawędź ma restrykcję}
            \State \Call{Apply}{krawędź.Początek, krawędź.Koniec}
        \EndIf
        \State krawędź $\gets$ krawędź.Następna
    \EndWhile
    \State{iter $\gets$ ostatnia krawędź wielokąta}
    \While{iter $\neq$ null $\wedge$ iter $\neq$ krawędź.Następna}
        \If{krawędź ma restrykcję}
            \State \Call{Apply}{krawędź.Koniec, krawędź.Początek}
        \EndIf
        \State krawędź $\gets$ krawędź.Poprzednia
    \EndWhile
    \If{wszystkie restrykcje są spełnione}
        \State \textbf{return}
    \Else
        \State{wycofaj dokonane zmiany}
    \EndIf
\EndFunction 
\end{algorithmic}
\caption{Pseudokod algorytmu propagacji restrykcji krawędzi. Krawędzie znajdują się na dwukierunkowej liście (\texttt{LinkedList})}
\end{algorithm}
\begin{figure}[H]
    \centering
    \includegraphics[width=12cm]{propagation-fixed.pdf}
    \caption{Schemat propagacji ograniczeń}
\end{figure}
Wybór kierunku propagacji ograniczeń jest umotywowany stałą pozycją pierwszego wierzchołka.

Powyższy algorytm jest wykonywany dla wszystkich krawędzi wielokąta, przy czym priorytet wyboru jest następujący:
\begin{enumerate}
    \item krawędzie bez restrykcji,
    \item krawędzie ustalone jako pionowe/poziome,
    \item krawędzie ustalonej długości.
\end{enumerate}
Powodem takiego uszeregowania krawędzi jest poziom swobody ich wierzchołków. Powyższe priorytety zrealizowane są poprzez sortowanie kubełkowe krawędzi oraz wykonywanie algorytmu dla kolejnych krawędzi aż do osiągnięcia sukcesu. W związku z tym złożoność optymistyczna algorytmu wynosi $O(n)$, zaś pesymistyczna -- $O(n^2)$, gdzie $n$ oznacza ilość krawędzi wielokąta.
\end{document}